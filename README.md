# ressources

Various resources about AgTechs

by Philippe Pary − philippe@pop.eu.com

## Misc

* Séminaire _Agricultures du futur_ de février 2019 : <https://plateforme-agrifutur.wixsite.com/5fevrier2019>
* Chaîne vidéo de Thierry Baillet _Agriculteur d’aujourd’hui_ : <https://www.youtube.com/channel/UCKpXbgTztfWrrNLx4w1EYKw>
* Site d’information gouvernemental sur l’emploi agricole : <https://agriculture.gouv.fr/actifagri-de-lemploi-lactivite-agricole-determinants-dynamiques-et-trajectoires>
