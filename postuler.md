# Trouver du travail ou un stage

Philippe Pary, décembre 2019

## Se préparer

### Avoir une adresse mail correcte

* Avoir un mail propre, on évite jordanleking59231@caramail.com
* Avoir son nom qui s’affiche correctement (identité d’expéditeur dans le client mail)

### Se contrôler en ligne

**STALKEZ VOUS**

* Via google
* Sur facebook
* Sur snapchat
* Sur twitter
* Partout

Traquez-vous, demandez à vos amis de vous traquer. Les RH sont les pires stalkers du monde (mais bon, parfois c’est payant de laisser traîner des trucs que vous assumez, genre des photos au carnaval de Dunkerque. Ça augmente le capital sympathie)

Un peu d’exercice

### Avoir un beau CV

* Avoir une belle version **PDF** … ⚠ vérifiez que la version s’imprime correctement en noir et blanc
    * indispensable hélas, en plus il sera imprimé …
* Facultatif: avoir un CV en ligne. Mieux encore, avec une adresse du genre cv.mondomaineperso.fr
    * c’est votre preuve de compétence HTML/CSS :)

## Être présent en ligne pour être contacté

### LinkedIn

https://www.linkedin.com

Réseau social d’entreprise

Vous créez tous un compte tout de suite. Vous ajoutez tout le personnel de PopSchool, Arnaud Flament et les personnes travaillant à la maison syndicale que vous avez rencontrées et vos responsables de projets collectifs. Et votre famille si vous la trouvez dessus.

### Les Job Boards

Pour trouver du travail, vous pourrez ignorer Pole Emploi.

Les job boards sont des sites webs, majoritairement privés (sauf l’APEC) et gratuits (conformément à la loi) proposant des offres d’emploi et où les recruteurs peuvent découvrir votre CV.

Allez vous créer un compte sur les sites suivants:
* indeed.fr
* Association Française d’Agriculture Urbaine Professionnelle (AFAUP) : http://www.afaup.org/jobs/

Autres jobs boards connus mais pas encore creusés pour l’AgTech:
* monster.fr
* keljob.com
* apec.fr (c’est un peu en lien avec Pole Emploi en fait …)
* regionsjob.com
* cadremploi.fr
* meteojob.com

Vous allez être contactés dans les semaines qui viennent, n’oubliez pas de répondre. Ne fermez jamais définitivement une porte !

### Être présent en ligne

* être actif sur twitter (AgTech, consultez ma liste : https://twitter.com/Gnuyo/lists/agtech
* participer à un projet libre AgTech, il doit pas y en avoir tant et ça vous apportera une vraie visibilité et du réseau
* créer des tutos texte, compléter wikipédia … Là c’est à indiquer sur le CV et ça consolidera vos savoir
* avoir une chaîne youtube. Idem, pour le CV et la visibilité. Peut-être un peu de réseautage si vous êtes suivis !

Si vous êtes actifs sur Internet et que vous indiquez être en recherche d’emploi, vous serez contactés.

## Postuler

### Avoir des nerfs

Parfois postuler c’est long. On ne compte pas les élèves qui ont eu à postuler une centaine de fois avant de trouver leur job.

La plupart du temps, vous resterez sans réponse. Relancez poliment jusqu’à ce que vous en ayez assez

On vous dira parfois non. Demandez les raisons, précisez que vous restez disponibles ou demandez s’il n’existe pas un poste par ailleurs.

Si on vous dit oui, soyez réactifs :)

### Tenir la liste

Créez un tableur dans lequel vous allez noter le nom de l’entreprise, quelques informations et chaque contact que vous avez effectué (la date, le type de contact, une note…)

Le but est d’éviter de postuler deux fois à la même annonce et gérer vos relances dans le temps

➡ On crée le tableur tout de suite

### Intitulés des postes

Aïe …

Il n’y a pas vraiment d’intitulés de poste pour le moment. Voici quelques mots-clefs:
* AgTech
* Agritech
* rtk / gpsd (si vous tapez GPS, vous avez les annonces de livreurs de pizza…)
* lora / sigfox
* IoT
* canbus / isobus

Soyez très attentifs à la veille menée par Dorothée sur #jobs, parfois les temps pour postuler sont courts

Suivez un maximum d’acteurs de l’AgTech et le mot-clef #AgTech sur LinkedIn

Vous pouvez mener des recherches sur les coopératives agricoles, aussi bien sur leurs sites webs que sur les jobboards

#### Quand postuler

Si vous avez 50% des compétences techniques demandées. Vous pouvez totalement ignorer le reste (exigence d’expérience, de diplôme etc.)

Vous pouvez signer un CDI là-maintenant-tout-de-suite-cassez-vous-de-là-que-je-prenne-des-vacances. Bref, vous pouvez quitter PopSchool si on vous propose un poste de dev tout de suite :)

#### Maraude

Aller sur les annuaires de coopératives, visiter leurs sites web. Souvent on y trouve une section emploi.

#### Candidature libre

Vous pouvez envoyer un CV à toute entreprise qui vous semble intéressante même si elle ne recrute pas. Elle pourrait transmettre votre CV à des confrères !

### Que dire ?

**Faites très attention à votre orthographe**

* Que vous cherchez un stage
* Des détails sur le stage : durée (2 mois minimum), mode (temps plein), éventuellement rémunération (à vous de voir entre rien, facultatif ou 550€/mois)
* Évoquez PopSchool (6 mois, 4 mois de cours, 2 mois de stage, création d’un titre pro niveau 3 en AgTech)
* Personnalisez le mail en montrant que vous vous êtes un minimum intéressé à l’entreprise (rien n’est plus gonflant que d’avoir l’impression que le mail est un copier/coller)
* Dites du bien de vous en quelques mots
* Dites au revoir (Bonne journée, Cordialement, Bien à vous …)

Points d’attention :  
* Inutile de dire votre nom : il apparaît dans le client mail
* Je vous ai dit de faire attention à l’orthographe ?

Créez-vous un template de mail : tous les textes sont prêts et relus, il ne vous reste plus qu’à remplir la section personnalisée sur l’entreprise (sans faire trop de faute d’orthographe, on se comprend ? :))

#### Exemples concrets

(here be exemples concrets from my inbox)

## Conseils

* Postulez beaucoup
* Tenez une liste des entreprises contactées pour éviter les doublons
* N’ayez pas peur : vous ne serez jamais pris si vous ne postulez pas
* Postulez énormément, genre plusieurs fois par jour
* Soyez vous-même, ne vous forcez pas à *faire des phrases*
* Sur votre CV, n’hésitez pas à mettre vos expériences pro même en *job de merde*. Ils prouvent votre capacité à travailler même sur des jobs peu intéressants; votre capacité à vous lever tôt, avoir une hiérarchie pressante, des contraintes de productivité etc.
* Postulez sans arrêt, hein
