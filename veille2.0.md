# La veille 2.0

6 janvier 2019, Philippe Pary

## Constat

* vous avez merdé sur la veille
	* c’était pas faute de vous en avoir parlé
	* vous allez payer ça TRÈS cher dans votre carrière
* on souhaite créer un catalogue PopSchool AgTech des IoT existants pour l’AgTech
	* vous allez le créer et ça sera votre veille …

## Planning

* 6 janvier: ah ah :)
* 7 janvier: trop court, veille _normale_
* 8 janvier: Anaïs
* 9 janvier: Killian
* 10 janvier: Vincent

* 13 janvier: Adrien
* 14 janvier: Mohammed
* 15 janvier: (après-midi) Thomas
* 16 janvier: Anaïs
* 17 janvier: Louise

* 20 janvier: Brahim
* 21 janvier: (après-midi) Manu
* 22 janvier: (après-midi) Thomas
* 23 janvier: Killian
* 24 janvier: Adrien

* 27 janvier: Vincent
* 28 janvier: Louise
* 29 janvier: Mohammed
* 30 janvier: Brahim
* 31 janvier: Manu

* 3 février: Killian (collectif autorisé)
* 4 février: Adrien (collectif autorisé)
* 5 février: Louise (collectif autorisé)
* 6 février: Thomas (collectif autorisé)
* 7 février: rien (débrief de promo)

## Un peu de méthodo…

Les attentes de PopSchool:
* Objectifs de l’objet
* Description de la société conceptrice (deux mots)
* Circuits de distribution (_shut up and take my money !_ comment on achète ?)
* SAV (comment se passe le support)
* Des échelles de compétence pour
	* installer
	* utiliser
	* maintenir
* Les points forts
* Les points faibles
* Les technologies utilisées (en bas de document hein)
* Une appréciation de A à E avec un petit commentaire

Évidemment certaines informations pourraient être lacunaires (comment noter si on n’a pas pu tester ? être exhaustif quand on a 5 jours pour faire une fiche ? …)

➡ on veut créer des fiches, c’est l’objectif 0. Leur qualité est souhaitée, mais ne doit pas entraver leur existance

## Création d’un template

Tous ensemble, convenons de la création d’un template !

## La présentation et évaluation

* vous ferez imprimer la fiche, un exemplaire par élève plus un pour PopSchool
* vous préparez des slides
	* si vous ne savez pas comment les remplir, reprenez tel quel les textes de la fiche !
* vous parlerez 3 minutes chronométrées
* vous serez évalués par vos camarades entre 5 et 0
	* un point pour la fiche
	* un point pour les slides
	* un point pour la prise de parole
	* deux points d’appréciation

## Récompenses

Le but est de vous challanger un peu. Il y aura récompense pour tous ceux qui auront joué le jeu :
* un tas de récompenses plus ou moins égales (une par participant)
* le premier choisi en premier, le second en second, le troisième en troisième etc.
* si vous voulez avoir le lot qui vous plaît le plus, va falloir bosser :D

## IoT / logiciels étudiés

Communiquez entre vous pour vous répartir les objets via le canal #veille

On peut le faire dès ce matin si ça vous chante :)
